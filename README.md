## Semantic versioning


## Start a redis-based worker
```
Rscript redisWorker.R localhost RTolMicroServRedis
```

## Start an HTTP service
```
DEBUG=app:info TASK_QUEUE=RTolMicroServRedis npm start
```

```
TASK_QUEUE=FAST_AIA REDIS_HOST=redis.host.com HOST_PORT=3000 docker-compose up -d
```